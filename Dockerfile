FROM alpine:3.8

WORKDIR /home/
COPY ms-push.bin .
RUN chmod +x ms-push.bin

COPY config ./config

EXPOSE 80
CMD ["./ms-push.bin"]