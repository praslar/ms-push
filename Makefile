PROJECT_NAME=ms-push
BUILD_VERSION=$(shell cat VERSION)
DOCKER_IMAGE=$(PROJECT_NAME):$(BUILD_VERSION)
GO_BUILD_ENV=CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on

.SILENT:

build:
	$(GO_BUILD_ENV) go build -v -o $(PROJECT_NAME).bin cmd/main.go

compose_prod: docker
	BUILD_VERSION=$(BUILD_VERSION) docker-compose up  --build --force-recreate -d

docker_build:
	docker build -t $(DOCKER_IMAGE) .;

docker_postbuild:
	rm -rf $(PROJECT_NAME).bin 2> /dev/null;\

docker: build docker_build docker_postbuild
