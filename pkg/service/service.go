package service

import (
	"context"
	"fmt"
	"ms-push/pkg/model"
	"ms-push/pkg/repo"
	"ms-push/pkg/utils"
	"sync"
	"time"
)

// MsPushService describes the service.
type MsPushService interface {
	// Add your methods here, e.x:
	Status(ctx context.Context, name string) (rs interface{}, err error)
	Consumer(ctx context.Context, req model.Consumer) (rs interface{}, err error)
}

type basicMsPushService struct {
	pgDB   repo.PostgresDatabase
	health *HealthStatus
}
type HealthStatus struct {
	name      string
	version   string
	startedAt time.Time
}

var once = sync.Once{}
var singleton *HealthStatus

func (b *basicMsPushService) Status(ctx context.Context, name string) (rs interface{}, err error) {
	// TODO implement the business logic of Status
	rs = model.HealthCheckResponse{
		Name:    b.health.name,
		Uptime:  time.Now().Sub(singleton.startedAt).String(),
		Version: b.health.version,
	}
	return rs, err
}

func (b *basicMsPushService) Consumer(ctx context.Context, req model.Consumer) (rs interface{}, err error) {
	switch req.Body.Topic {
	//case utils.PushCategory:
	//	request := model.PushCategory{}
	//	return request, nil
	//case utils.PushProduct:
	//	request := model.PushProduct{}
	//	return request, nil
	//case utils.PushProductVariant:
	//	request := model.PushProductVariant{}
	//	return request, nil
	//case utils.PushOrder:
	//	request := model.PushOrder{}
	//	return request, nil
	//case utils.PushOrderItem:
	//	request := model.PushOrderItem{}
	//	return request, nil
	//case utils.PushCustomer:
	//	request := model.PushCustomer{}
	//	return request, nil
	//case utils.PushShop:
	//	request := model.PushShop{}
	//	return request, nil
	//case utils.PushReview:
	//	request := model.PushReview{}
	//	return request, nil
	default:
		return nil, fmt.Errorf("topic not found in this service")
	}
}

// NewBasicMsPushService returns a naive, stateless implementation of MsPushService.
func NewBasicMsPushService(db repo.PostgresDatabase) MsPushService {
	once.Do(func() {
		singleton = &HealthStatus{
			name:      "ms-push/",
			startedAt: time.Now(),
			version:   utils.GetEnv().Version,
		}
	})
	return &basicMsPushService{pgDB: db, health: singleton}
}

// New returns a MsPushService with all of the expected middleware wired in.
func New(middleware []Middleware, db repo.PostgresDatabase) MsPushService {
	var svc MsPushService = NewBasicMsPushService(db)
	for _, m := range middleware {
		svc = m(svc)
	}
	return svc
}
