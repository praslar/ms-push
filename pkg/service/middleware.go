package service

import (
	"context"
	log "github.com/go-kit/kit/log"
	model "ms-push/pkg/model"
)

// Middleware describes a service middleware.
type Middleware func(MsPushService) MsPushService

type loggingMiddleware struct {
	logger log.Logger
	next   MsPushService
}

// LoggingMiddleware takes a logger as a dependency
// and returns a MsPushService Middleware.
func LoggingMiddleware(logger log.Logger) Middleware {
	return func(next MsPushService) MsPushService {
		return &loggingMiddleware{logger, next}
	}

}

func (l loggingMiddleware) Status(ctx context.Context, name string) (rs interface{}, err error) {
	defer func() {
		l.logger.Log("method", "Status", "name", name, "rs", rs, "err", err)
	}()
	return l.next.Status(ctx, name)
}
func (l loggingMiddleware) Consumer(ctx context.Context, req model.Consumer) (rs interface{}, err error) {
	defer func() {
		l.logger.Log("method", "Consumer", "req", req, "rs", rs, "err", err)
	}()
	return l.next.Consumer(ctx, req)
}
