package model

import (
	"fmt"
	log "github.com/go-kit/kit/log"
	uuid "github.com/google/uuid"
	postgres "ms-push/pkg/db/postgres"
	"time"
)

var logger log.Logger

// MsPush describes the structure.
type BaseModel struct {
	ID        uuid.UUID  `gorm:"primary_key;type:uuid;default:uuid_generate_v4()" json:"id"`
	CreatorID uuid.UUID  `json:"creator_id"`
	UpdaterID uuid.UUID  `json:"updater_id"`
	CreatedAt time.Time  `gorm:"column:created_at;default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt time.Time  `gorm:"column:updated_at;default:CURRENT_TIMESTAMP" json:"updated_at"`
	DeletedAt *time.Time `json:"deleted_at" sql:"index"`
}
type HealthCheckResponse struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Uptime  string `json:"uptime"`
}
type Pagination struct {
	Page     int
	Size     int
	NextPage int
	LastPage int
	Total    int
}
type GetAllResponse struct {
	All        interface{} `json:"all"`
	Pagination *Pagination `json:"-"`
}

func AutoMigration() (err error) {
	dbPublic, err := postgres.GetDatabase("default")
	if err != nil {
		return err
	}
	_, err = dbPublic.DB().Exec("CREATE EXTENSION IF NOT EXISTS \"uuid-ossp\"")
	if err != nil {
		return fmt.Errorf("error while creating DB extension 'uuid-ossp': %s", err)
	}
	t := dbPublic.AutoMigrate()
	return t.Error
}


type Consumer struct {
	Body ConsumerBody
	//Param  DemoParam
	//Scheme DemoScheme
}

type ConsumerBody struct {
	Topic   string `json:"topic"`
	Payload string `json:"payload"`
}
