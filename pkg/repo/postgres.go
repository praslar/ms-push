package repo

import (
	"github.com/jinzhu/gorm"
)

func NewBasicPostgresDatabase(db *gorm.DB) PostgresDatabase {
	return &basicPostgresDatabase{
		db: db,
	}
}

type basicPostgresDatabase struct {
	db *gorm.DB
}


type PostgresDatabase interface {
// Add your db method here,
// e.x: Create(s User)(rs User, err error)",
}