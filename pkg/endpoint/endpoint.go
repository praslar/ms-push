package endpoint

import (
	"context"
	endpoint "github.com/go-kit/kit/endpoint"
	model "ms-push/pkg/model"
	service "ms-push/pkg/service"
)

// StatusRequest collects the request parameters for the Status method.
type StatusRequest struct {
	Name string `json:"name"`
}

// StatusResponse collects the response parameters for the Status method.
type StatusResponse struct {
	Rs  interface{} `json:"rs"`
	Err error       `json:"err"`
}

// MakeStatusEndpoint returns an endpoint that invokes Status on the service.
func MakeStatusEndpoint(s service.MsPushService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(StatusRequest)
		rs, err := s.Status(ctx, req.Name)
		return StatusResponse{
			Err: err,
			Rs:  rs,
		}, nil
	}
}

// Failed implements Failer.
func (r StatusResponse) Failed() error {
	return r.Err
}
func (r StatusResponse) Success() interface{} {
	return r.Rs
}

// ConsumerRequest collects the request parameters for the Consumer method.
type ConsumerRequest struct {
	Req model.Consumer `json:"req"`
}

// ConsumerResponse collects the response parameters for the Consumer method.
type ConsumerResponse struct {
	Rs  interface{} `json:"rs"`
	Err error       `json:"err"`
}

// MakeConsumerEndpoint returns an endpoint that invokes Consumer on the service.
func MakeConsumerEndpoint(s service.MsPushService) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(ConsumerRequest)
		rs, err := s.Consumer(ctx, req.Req)
		return ConsumerResponse{
			Err: err,
			Rs:  rs,
		}, nil
	}
}

// Failed implements Failer.
func (r ConsumerResponse) Failed() error {
	return r.Err
}
func (r ConsumerResponse) Success() interface{} {
	return r.Rs
}

// Failure is an interface that should be implemented by response types.
// Response encoders can check if responses are Failer, and if so they've
// failed, and if so encode them using a separate write path based on the error.
type Failure interface {
	Failed() error
}
type Success interface {
	Success() interface{}
}

// Status implements Service. Primarily useful in a client.
func (e Endpoints) Status(ctx context.Context, name string) (rs interface{}, err error) {
	request := StatusRequest{Name: name}
	response, err := e.StatusEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(StatusResponse).Rs, response.(StatusResponse).Err
}

// Consumer implements Service. Primarily useful in a client.
func (e Endpoints) Consumer(ctx context.Context, req model.Consumer) (rs interface{}, err error) {
	request := ConsumerRequest{Req: req}
	response, err := e.ConsumerEndpoint(ctx, request)
	if err != nil {
		return
	}
	return response.(ConsumerResponse).Rs, response.(ConsumerResponse).Err
}
