package http

import (
	"context"
	"encoding/json"
	"errors"
	http "github.com/go-kit/kit/transport/http"
	handlers "github.com/gorilla/handlers"
	mux "github.com/gorilla/mux"
	schema "github.com/gorilla/schema"
	response "github.com/praslar/lib/response"
	endpoint "ms-push/pkg/endpoint"
	http1 "net/http"
)

// makeStatusHandler creates the handler logic
func makeStatusHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("GET").Path("/api/status").Handler(handlers.CORS(handlers.AllowedMethods([]string{"GET"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.StatusEndpoint, decodeStatusRequest, encodeStatusResponse, options...)))
}

// decodeStatusRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
// Add your request body/param/scheme here
func decodeStatusRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.StatusRequest{}
	err := decode(r, nil, nil, nil)
	return req, err
}

// encodeStatusResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeStatusResponse(ctx context.Context, w http1.ResponseWriter, res interface{}) (err error) {
	if f, ok := res.(endpoint.Failure); ok && f.Failed() != nil {
		response.Error(w, f.Failed(), http1.StatusInternalServerError)
		return nil
	}
	if f, ok := res.(endpoint.Success); ok && f.Success() != nil {
		response.JSON(w, http1.StatusOK, response.BaseResponse{Data: f.Success()})
		return nil
	}
	response.JSON(w, http1.StatusOK, response.BaseResponse{Data: res})
	return
}

// makeConsumerHandler creates the handler logic
func makeConsumerHandler(m *mux.Router, endpoints endpoint.Endpoints, options []http.ServerOption) {
	m.Methods("POST").Path("/api/consumer").Handler(handlers.CORS(handlers.AllowedMethods([]string{"POST"}), handlers.AllowedOrigins([]string{"*"}))(http.NewServer(endpoints.ConsumerEndpoint, decodeConsumerRequest, encodeConsumerResponse, options...)))
}

// decodeConsumerRequest is a transport/http.DecodeRequestFunc that decodes a
// JSON-encoded request from the HTTP request body.
// Add your request body/param/scheme here
func decodeConsumerRequest(_ context.Context, r *http1.Request) (interface{}, error) {
	req := endpoint.ConsumerRequest{}
	err := decode(r, nil, nil, nil)
	return req, err
}

// encodeConsumerResponse is a transport/http.EncodeResponseFunc that encodes
// the response as JSON to the response writer
func encodeConsumerResponse(ctx context.Context, w http1.ResponseWriter, res interface{}) (err error) {
	if f, ok := res.(endpoint.Failure); ok && f.Failed() != nil {
		response.Error(w, f.Failed(), http1.StatusInternalServerError)
		return nil
	}
	if f, ok := res.(endpoint.Success); ok && f.Success() != nil {
		response.JSON(w, http1.StatusOK, response.BaseResponse{Data: f.Success()})
		return nil
	}
	response.JSON(w, http1.StatusOK, response.BaseResponse{Data: res})
	return
}
func ErrorEncoder(_ context.Context, err error, w http1.ResponseWriter) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(err2code(err))
	json.NewEncoder(w).Encode(errorWrapper{Error: err.Error()})
}
func ErrorDecoder(r *http1.Response) error {
	var w errorWrapper
	if err := json.NewDecoder(r.Body).Decode(&w); err != nil {
		return err
	}
	return errors.New(w.Error)
}
func decode(r *http1.Request, param interface{}, schemas interface{}, body interface{}) (err error) {
	if param != nil {
		_ = schema.NewDecoder().Decode(param, r.URL.Query())
	}
	if schemas != nil {
		vars := mux.Vars(r)
		data, err := json.Marshal(vars)
		if err != nil {
			return err
		}
		err = json.Unmarshal(data, schemas)
		if err != nil {
			return err
		}
	}
	if body != nil {
		err = json.NewDecoder(r.Body).Decode(body)
		if err != nil {
			return err
		}
	}
	return err
}

// This is used to set the http status, see an example here :
// https://github.com/go-kit/kit/blob/master/examples/addsvc/pkg/addtransport/http.go#L133
func err2code(err error) int {
	return http1.StatusInternalServerError
}

type errorWrapper struct {
	Error string `json:"error"`
}
