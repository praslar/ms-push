package utils

const (
	PushCategory       = "push_uc:category"
	PushProduct        = "push_uc:product"
	PushProductVariant = "push_uc:product_variant"
	PushOrder          = "push_uc:order"
	PushOrderItem      = "push_uc:order_item"
	PushCustomer       = "push_uc:customer"
	PushShop           = "push_uc:shop"
	PushReview         = "push_uc:review"
)
